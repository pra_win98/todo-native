const COLOR = {
  blue: '#5591F5',
  white: '#fff',
  green: '#46AE62',
  lightGreen: '#CFE5D5',
  gray: '#D7D7D7',
};

export default COLOR;
