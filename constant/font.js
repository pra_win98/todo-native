const FONT = {
  black: 'Nunito-Black',
  bold: 'Nunito-Bold',
  extraBold: 'Nunito-ExtraBold',
  semiBold: 'Nunito-SemiBold',
  light: 'Nunito-Light',
  regular: 'Nunito-Regular',
};

export default FONT;
