import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Row from '../components/Row';

const TestScreen = () => {
  return (
    <View style={styles.container}>
      <Row />
    </View>
  );
};

export default TestScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    marginTop: 50,
    paddingHorizontal: 20,
  },
});
