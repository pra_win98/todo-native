import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useCallback, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import Header from '../components/Header';
import Row from '../components/Row';
import Todo from '../components/Todo';
import FONT from '../constant/font';
import COLOR from '../constant/color';

const TodoScreen = ({navigation}) => {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    navigation.addListener('focus', () => {
      AsyncStorage.getItem('todos')
        .then(data => {
          if (data !== null) {
            setTodos(JSON.parse(data));
          } else setTodos([]);
        })
        .catch(error => console.log(error));
    });
  }, []);

  const DeleteTodo = useCallback(
    id => {
      const newList = [...todos];
      const index = newList.findIndex(item => item.id === id);
      newList.splice(index, 1);
      setTodos(newList);
      AsyncStorage.setItem('todos', JSON.stringify(newList)).catch(err =>
        console.log(err.message),
      );
    },
    [todos],
  );

  const CompletedTask = (
    <FlatList
      style={{marginTop: 15}}
      data={todos.filter(item => item.done === true)}
      renderItem={({item, index}) => (
        <View style={{marginBottom: 15}}>
          <Row
            done={item.done}
            onDelete={() => DeleteTodo(item.id)}
            onPress={() =>
              navigation.navigate('SingleTodo', {
                todo: item,
                todos,
              })
            }
            onEdit={() =>
              navigation.navigate('EditTodo', {
                todos,
                id: item.id,
              })
            }>
            <Todo text={item.name} date={item.date} done={item.done} />
          </Row>
        </View>
      )}
    />
  );

  const remainingLength = todos.filter(item => item.done === false).length;
  const completedLength = todos.filter(item => item.done === true).length;

  const RemainingText = completedLength > 0 && remainingLength > 0 && (
    <Text style={styles.remaining}>
      Remaining Tasks <Text style={styles.boldText}>({remainingLength})</Text>
    </Text>
  );

  const IncompletedTask = (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={todos.filter(item => item.done === false)}
      renderItem={({item}) => (
        <View style={{marginBottom: 15}}>
          <Row
            onDelete={() => DeleteTodo(item.id)}
            onPress={() =>
              navigation.navigate('SingleTodo', {
                todo: item,
                todos,
              })
            }
            onEdit={() =>
              navigation.navigate('EditTodo', {
                todos,
                id: item.id,
              })
            }>
            <Todo text={item.name} date={item.date} done={item.done} />
          </Row>
        </View>
      )}
    />
  );

  const thingsToShow = [CompletedTask, RemainingText, IncompletedTask];

  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity
        onPress={() => navigation.navigate('AddTodo', {todos})}
        style={styles.square}>
        <Icon name={'plus'} color="#fff" size={25} />
      </TouchableOpacity>
      <View style={styles.wrapper}>
        <Header />

        {todos.length > 0 ? (
          <FlatList
            data={thingsToShow}
            showsVerticalScrollIndicator={false}
            renderItem={({_, index}) => thingsToShow[index]}
          />
        ) : (
          <View style={{marginTop: 200}}>
            <Text
              style={{
                fontFamily: FONT.extraBold,
                textAlign: 'center',
                fontSize: 33,
                color: COLOR.blue,
              }}>
              No Tasks Found!
            </Text>
            <Text style={{textAlign: 'center'}}>
              Add new task by click plus icon
            </Text>
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

export default TodoScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2',
  },
  wrapper: {
    paddingHorizontal: 20,
    height: '100%',
  },
  square: {
    position: 'absolute',
    width: 64,
    height: 64,
    backgroundColor: COLOR.blue,
    borderRadius: 18,
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 20,
    right: 20,
    zIndex: 10,
  },
  remaining: {
    fontSize: 22,
    fontFamily: FONT.light,
    color: '#000',
    marginBottom: 10,
  },
  boldText: {
    fontFamily: FONT.extraBold,
  },
  rowBack: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: '80%',
    borderRadius: 15,
    backgroundColor: COLOR.blue,
  },
  action: {
    height: '100%',
    marginRight: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
