import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import React, {useState} from 'react';
import {
  Alert,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  SafeAreaView,
} from 'react-native';
import DatePicker from '../components/DatePicker';
import FONT from '../constant/font';
import COLOR from '../constant/color';
import CheckBox from '@react-native-community/checkbox';

const AddTodoScreen = ({navigation, route}) => {
  const {todos, id} = route.params;

  var newTodo = todos.filter(item => item.id === id)[0];
  const [todo, setTodo] = useState({...newTodo, date: moment(newTodo.date)});
  const {name, description, priority, date, done} = todo;
  const [show, setShow] = useState(false);

  const editTodo = id => {
    if (name == '' || description == '' || priority == '') {
      Alert.alert('Error', 'Please fill all the fields.');
    } else {
      let newList = todos.map(item => {
        if (item.id === id) {
          return todo;
        } else {
          return item;
        }
      });

      AsyncStorage.setItem('todos', JSON.stringify(newList))
        .then(() => {
          // setTodos(newList);
          navigation.navigate('Todo');
        })
        .catch(err => console.log(err.message));
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapper}>
        <View style={styles.box}>
          <Text style={styles.label}>Name</Text>
          <TextInput
            style={styles.input}
            placeholder="Enter name of your new task"
            value={name}
            onChangeText={text => setTodo({...todo, name: text})}
          />
        </View>
        <View style={styles.box}>
          <Text style={styles.label}>Discription</Text>
          <TextInput
            style={styles.input}
            placeholder="Describe the task "
            value={description}
            onChangeText={text => setTodo({...todo, description: text})}
          />
        </View>

        <View style={styles.box}>
          <Text style={styles.label}>Priority</Text>
          <TextInput
            style={styles.input}
            placeholder="Select your priority"
            value={priority}
            onChangeText={text => setTodo({...todo, priority: text})}
          />
        </View>
        <View style={styles.box}>
          <Text style={styles.label}>Due Date</Text>
          <DatePicker
            date={date}
            setDate={date => setTodo({...todo, date})}
            show={show}
            setShow={setShow}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          {/* <CheckBox /> */}
          <Text style={styles.label}>Mark as done</Text>
          <CheckBox
            disabled={false}
            value={done}
            onValueChange={newValue => setTodo({...todo, done: newValue})}
          />
        </View>
        <TouchableOpacity style={styles.submit} onPress={() => editTodo(id)}>
          <Text style={styles.btnText}>Submit</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default AddTodoScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.white,
  },
  wrapper: {
    marginTop: 20,
    paddingHorizontal: 20,
  },
  box: {
    marginBottom: 15,
  },
  label: {
    fontFamily: FONT.semiBold,
    fontSize: 17,
    color: 'black',
    marginLeft: 5,
    marginBottom: 5,
  },
  input: {
    backgroundColor: '#f8f8f8',
    paddingHorizontal: 15,
    borderRadius: 10,
  },
  date: {
    height: 50,
    justifyContent: 'center',
  },
  submit: {
    backgroundColor: COLOR.blue,
    paddingVertical: 8,
    borderRadius: 10,
    marginTop: 20,
  },
  btnText: {
    color: COLOR.white,
    fontFamily: FONT.semiBold,
    fontSize: 22,
    textAlign: 'center',
  },
});
