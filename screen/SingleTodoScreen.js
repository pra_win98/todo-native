import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import FONT from '../constant/font';
import COLOR from '../constant/color';
import Icon from 'react-native-vector-icons/AntDesign';
import moment from 'moment';

const SingleTodoScreen = ({navigation, route}) => {
  let {todo, todos} = route.params;

  const DeleteTodo = id => {
    const newList = [...todos];
    const index = newList.findIndex(item => item.id === id);
    newList.splice(index, 1);

    AsyncStorage.setItem('todos', JSON.stringify(newList))
      .then(() => {
        navigation.navigate('Todo');
      })
      .catch(err => console.log(err.message));
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapper}>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.title}>{todo.name}</Text>
          {todo.done ? (
            <Icon
              name="checkcircle"
              color={COLOR.green}
              size={33}
              style={{marginTop: 5}}
            />
          ) : (
            <View style={{alignItems: 'center'}}>
              <Text style={{marginTop: 10, fontFamily: FONT.bold}}>
                {moment(todo.date).format('MMM DD')}
              </Text>
            </View>
          )}
        </View>
        <Text
          style={{
            fontFamily: FONT.extraBold,
            color: COLOR.blue,
            fontSize: 18,
            marginVertical: 5,
          }}>
          {todo.priority.toUpperCase()}
        </Text>
        <Text style={styles.text}>{todo.description}</Text>

        <View style={{flexDirection: 'row'}}>
          {todo.done === false && (
            <TouchableOpacity
              style={[styles.btn, {backgroundColor: COLOR.blue}]}
              onPress={() =>
                navigation.navigate('EditTodo', {
                  todos,
                  id: todo.id,
                })
              }>
              <Text style={styles.butText}>Edit</Text>
              <Icon name="edit" size={20} color={'#fff'} />
            </TouchableOpacity>
          )}
          <TouchableOpacity
            style={[styles.btn, {backgroundColor: '#F55555'}]}
            onPress={() => DeleteTodo(todo.id)}>
            <Text style={styles.butText}>Delete</Text>
            <Icon name="edit" size={20} color={'#fff'} />
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default SingleTodoScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2',
  },
  wrapper: {
    marginTop: 50,
    paddingHorizontal: 20,
    height: '100%',
  },
  title: {
    fontFamily: FONT.bold,
    fontSize: 24,
    color: '#000',
    width: '80%',
    marginRight: 'auto',
  },
  text: {
    fontSize: 18,
    fontFamily: FONT.regular,
  },
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 120,
    justifyContent: 'space-around',
    padding: 12,
    borderRadius: 15,
    marginTop: 25,
    marginRight: 10,
  },
  butText: {
    color: COLOR.white,
    fontSize: 18,
    fontFamily: FONT.bold,
  },
});
