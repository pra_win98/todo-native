import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import FONT from '../constant/font';
import COLOR from '../constant/color';

const HomeScreen = ({navigation}) => {
  function RemoveTodo() {
    AsyncStorage.removeItem('todos')
      .then(() => console.log('Todos deleted'))
      .catch(err => console.log('Error'));
  }

  // React.useEffect(() => {
  //   RemoveTodo();
  // }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapper}>
        <View style={styles.square}>
          <Icon name={'check'} color="#fff" size={25} />
        </View>
        <Text style={styles.title}>
          Welcome to <Text style={styles.boldText}>My Todo</Text>
        </Text>
        <Text style={styles.smallText}>
          My Todo helps you stay organized and perform your task much faster
        </Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('Todo')}
          style={styles.button}>
          <Text style={styles.buttonText}>Try Demo</Text>
        </TouchableOpacity>
        <Text style={styles.buttonText}>No thanks</Text>
      </View>
    </SafeAreaView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.white,
  },
  wrapper: {
    marginTop: 'auto',
    marginBottom: 'auto',
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  square: {
    width: 64,
    height: 64,
    backgroundColor: COLOR.blue,
    borderRadius: 18,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    width: 135,
    textAlign: 'center',
    lineHeight: 30,
    fontFamily: FONT.light,
    marginVertical: 35,
  },
  boldText: {
    fontFamily: FONT.extraBold,
    color: '#4F4F4F',
  },
  smallText: {
    fontSize: 15,
    width: 278,
    textAlign: 'center',
    fontFamily: FONT.light,
  },
  button: {
    width: 197,
    height: 57,
    backgroundColor: '#D9E6FA',
    borderRadius: 18,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    marginTop: 100,
  },
  buttonText: {
    color: COLOR.blue,
    fontSize: 16,
    fontFamily: FONT.bold,
  },
});
