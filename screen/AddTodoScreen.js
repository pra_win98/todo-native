import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import React, {useState} from 'react';
import {
  Alert,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  SafeAreaView,
} from 'react-native';
import DatePicker from '../components/DatePicker';
import FONT from '../constant/font';
import COLOR from '../constant/color';

const AddTodoScreen = ({navigation, route}) => {
  const {todos} = route.params;
  const [todo, setTodo] = useState({
    date: moment(),
    name: '',
    description: '',
    priority: '',
  });
  const {name, description, priority, date} = todo;
  const [show, setShow] = useState(false);

  const addTodo = () => {
    if (name == '' || description == '' || priority == '') {
      Alert.alert('Error', 'Please fill all the fields.');
    } else {
      const newTodo = {
        name,
        description,
        date,
        priority,
        done: false,
        id: Math.floor(Math.random() * 1000),
      };

      const newList = [...todos, newTodo];
      AsyncStorage.setItem('todos', JSON.stringify(newList))
        .then(() => {
          // setTodos(newList);
          navigation.navigate('Todo');
        })
        .catch(err => console.log(err.message));
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapper}>
        <View style={styles.box}>
          <Text style={styles.label}>Name</Text>
          <TextInput
            style={styles.input}
            placeholder="Enter name of your new task"
            value={name}
            onChangeText={text => setTodo({...todo, name: text})}
          />
        </View>
        <View style={styles.box}>
          <Text style={styles.label}>Discription</Text>
          <TextInput
            style={styles.input}
            placeholder="Describe the task "
            value={description}
            onChangeText={text => setTodo({...todo, description: text})}
          />
        </View>

        <View style={styles.box}>
          <Text style={styles.label}>Priority</Text>
          <TextInput
            style={styles.input}
            placeholder="Select your priority"
            value={priority}
            onChangeText={text => setTodo({...todo, priority: text})}
          />
        </View>
        <View style={styles.box}>
          <Text style={styles.label}>Due Date</Text>
          <DatePicker
            date={date}
            setDate={date => setTodo({...todo, date})}
            show={show}
            setShow={setShow}
          />
        </View>
        <TouchableOpacity style={styles.submit} onPress={addTodo}>
          <Text style={styles.btnText}>Add Todo</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default AddTodoScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.white,
  },
  wrapper: {
    marginTop: 20,
    paddingHorizontal: 20,
  },
  box: {
    marginBottom: 15,
  },
  label: {
    fontFamily: FONT.semiBold,
    fontSize: 17,
    color: 'black',
    marginLeft: 5,
    marginBottom: 5,
  },
  input: {
    backgroundColor: '#f8f8f8',
    paddingHorizontal: 15,
    borderRadius: 10,
  },
  date: {
    height: 50,
    justifyContent: 'center',
  },
  submit: {
    backgroundColor: COLOR.blue,
    paddingVertical: 8,
    borderRadius: 10,
    marginTop: 20,
  },
  btnText: {
    color: COLOR.white,
    fontFamily: FONT.semiBold,
    fontSize: 22,
    textAlign: 'center',
  },
});
