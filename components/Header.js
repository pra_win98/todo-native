import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FIcon from 'react-native-vector-icons/FontAwesome5';
import FONT from '../constant/font';
import COLOR from '../constant/color';

const Header = () => {
  return (
    <View style={styles.container}>
      <Icon name="menu" size={33} color="#8C8C8C" />
      <Text style={styles.title}>My Todo</Text>
      <FIcon name="bell" size={25} color="#8C8C8C" solid />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  title: {
    fontFamily: FONT.black,
    fontSize: 24,
    color: '#505050',
  },
});
