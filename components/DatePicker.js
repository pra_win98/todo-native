import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';

const DatePicker = ({date, setDate, show, setShow}) => {
  const onChange = (event, selectedDate) => {
    const currentDate = moment(selectedDate) || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  return (
    <TouchableHighlight
      activeOpacity={1}
      style={{borderRadius: 10}}
      onPress={() => setShow(true)}>
      <View style={[styles.date, styles.input]}>
        <Text>{date.format('Do MMM YYYY')}</Text>
        {show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={new Date(date)}
            minimumDate={new Date()}
            mode={'mode'}
            display="default"
            onChange={onChange}
          />
        )}
      </View>
    </TouchableHighlight>
  );
};

export default DatePicker;

const styles = StyleSheet.create({
  input: {
    backgroundColor: '#f8f8f8',
    paddingHorizontal: 15,
    borderRadius: 10,
  },
  date: {
    height: 50,
    justifyContent: 'center',
  },
});
