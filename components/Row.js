import React from 'react';
import {
  StyleSheet,
  Animated,
  View,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import {Swipeable, RectButton} from 'react-native-gesture-handler';
import COLOR from '../constant/color';
import Icon from 'react-native-vector-icons/FontAwesome';

const Row = ({children, onDelete, onEdit, onPress, done}) => {
  const swipeRef = React.useRef();

  const closeSwipable = () => {
    swipeRef?.current?.close();
  };

  const RightAction = ({icon, color, trans, onPress, Style}) => {
    return (
      <TouchableOpacity onPress={onPress} style={{flex: 1}}>
        <Animated.View
          style={[
            {
              transform: [{translateX: trans}],
              flex: 1,
              backgroundColor: color,
            },
            Style,
          ]}>
          <RectButton style={[styles.action]}>
            <Icon name={icon} color="#fff" size={25} />
          </RectButton>
        </Animated.View>
      </TouchableOpacity>
    );
  };

  const renderRightActions = (progress, dragX) => {
    const trans = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [1, 0],
      extrapolate: 'clamp',
    });
    return (
      <View
        style={{
          width: done ? 64 : 128,
          flexDirection: 'row',
          backgroundColor: COLOR.blue,
          justifyContent: 'space-around',
          borderTopRightRadius: 10,
          borderBottomRightRadius: 10,
        }}>
        {!done && (
          <RightAction
            icon="edit"
            color="#5591F5"
            trans={trans}
            onPress={() => {
              closeSwipable();
              onEdit();
            }}
          />
        )}
        <RightAction
          icon="trash-o"
          color="#5591F5"
          trans={trans}
          Style={styles.last}
          onPress={() => {
            closeSwipable();
            onDelete();
          }}
        />
      </View>
    );
  };

  return (
    <Swipeable ref={swipeRef} renderRightActions={renderRightActions}>
      <TouchableHighlight
        activeOpacity={0}
        style={{borderRadius: 15}}
        onPress={onPress}>
        {children}
      </TouchableHighlight>
    </Swipeable>
  );
};

export default Row;

const styles = StyleSheet.create({
  item: {
    backgroundColor: COLOR.white,
    padding: 20,
    // marginBottom: 15,
    borderRadius: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  action: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  last: {
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
});
