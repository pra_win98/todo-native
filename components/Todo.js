import React from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import FONT from '../constant/font';
import COLOR from '../constant/color';
import moment from 'moment';

const width = 0.9 * Dimensions.get('window').width;

const Todo = ({text, date, done}) => {
  return (
    <View
      style={{
        backgroundColor: done ? COLOR.lightGreen : COLOR.white,
        padding: 20,
        borderRadius: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        width,
      }}>
      <View style={styles.left}>
        <Icon
          name="checkcircle"
          color={done ? COLOR.green : COLOR.gray}
          size={18}
          style={{marginRight: 10}}
        />
        <Text style={styles.text}>{text}</Text>
      </View>
      <Text style={styles.date}>
        {done ? moment(date).fromNow(true) : moment(date).format('MMM DD')}
      </Text>
      {/* <Text style={styles.date}>
        {done
          ? formatDistanceToNow(new Date(2014, 6, 2))
          : format(new Date(), 'LLL d')}
      </Text> */}
    </View>
  );
};
// CFE5D5
export default Todo;

const styles = StyleSheet.create({
  left: {
    flexDirection: 'row',
  },
  date: {
    color: '#363636',
    fontFamily: FONT.regular,
  },
  text: {
    width: '75%',
    fontSize: 20,
    marginTop: -5,
    color: '#000',
    fontFamily: FONT.bold,
  },
});
