import { NavigationContainer } from '@react-navigation/native';
import {
  CardStyleInterpolators, createStackNavigator
} from '@react-navigation/stack';
import React from 'react';
import { StyleSheet } from 'react-native';
import 'react-native-gesture-handler';
import AddTodoScreen from './screen/AddTodoScreen';
import EditTodoScreen from './screen/EditTodoScreen';
import HomeScreen from './screen/HomeScreen';
import SingleTodoScreen from './screen/SingleTodoScreen';
import TodoScreen from './screen/TodoScreen';

const Stack = createStackNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {/* <Stack.Screen name="Test" component={TestScreen} /> */}
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Todo"
          component={TodoScreen}
          options={{
            headerShown: false,
            gestureDirection: 'horizontal',
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="AddTodo"
          component={AddTodoScreen}
          options={{headerTitle: 'Add New Task', headerTitleAlign: 'center'}}
        />
        <Stack.Screen
          name="EditTodo"
          component={EditTodoScreen}
          options={{headerTitle: 'Edit Task', headerTitleAlign: 'center'}}
        />
        <Stack.Screen
          name="SingleTodo"
          component={SingleTodoScreen}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({});
